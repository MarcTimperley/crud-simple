[![pipeline status](https://gitlab.com/MarcTimperley/crud-simple/badges/master/pipeline.svg)](https://gitlab.com/MarcTimperley/crud-simple/commits/master)

# CRUD-Simple - A simple Create Read Update and Delete Front-End

## Demo

A running demo of the application is available at https://marctimperley.gitlab.io/crud-simple

## Outline
This is a React project with a simple front end with demo CRUD functionality. It loads a JSON array of data and displays this as a stock list. The updates are not persisted and will be lost with a restart or reload of the application.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## To Do List

 - [ ] Create version using a database instead of local JSON database
 - [ ] Improve test cases
 - [X] Convert to using hooks
