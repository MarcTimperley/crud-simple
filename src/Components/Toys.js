import React, { useState } from 'react'

let toys = [
  {
    name: 'Lego',
    type: 'Construction',
    quantity: 12
  },
  {
    name: 'Car',
    type: 'Pretend',
    quantity: 5
  },
  {
    name: 'Purse',
    type: 'Pocket Money',
    quantity: 11
  },
  {
    name: 'Costume',
    type: 'Pretend',
    quantity: 15
  },
  {
    name: 'Controller - PS4',
    type: 'Video Games',
    quantity: 6
  }
]

function Toys() {
  const [toyList, setToyList] = useState(toys)
  console.log(toyList)
  const [ascending, setAscending] = useState('checked')
  const [removeToy, setRemoveToy] = useState('')
  const [newName, setNewName] = useState('')
  const [newType, setNewType] = useState('')
  const [newQuantity, setNewQuantity] = useState('')
  const onChangeName = event => {
    setNewName(event.target.value)
  }
  const onChangeType = event => {
    setNewType(event.target.value)
  }
  const onChangeQuantity = event => {
    setNewQuantity(event.target.value)
  }
  const onChangeRemove = event => {
    setRemoveToy(event.target.value)
  }
  const onSubmit = event => {
    if (newName && newType && newQuantity) {
      let newToy = { name: newName, type: newType, quantity: newQuantity }
      setToyList([...toyList, newToy])
      setNewName('')
      setNewType('')
      setNewQuantity('')
    }

    event.preventDefault()
  }
    const onRemoveToy = () => {
      if (!removeToy || removeToy === '') return
      console.log('removing ' + removeToy)
      setToyList(toyList.filter((toy) => {
          return toy.name !== removeToy
        }))
      setRemoveToy('')
    }
  const toggleAscending = () => {
    ascending === 'checked' ? setAscending('unchecked') : setAscending('checked')
  }

  let toylist = toyList
  console.log(toylist)
  if (ascending === 'checked') {
    toylist = toylist.sort((a, b) => {
      return (a.name > b.name) ? 1 : -1
    })
      .map((item) =>
        <tr key={item.name + 'asc'}>{Object.values(item).map(value => {
          return <td key={item.name + 'asc' + value}>{value}</td>
        })}</tr>
      )
  } else {
    toylist = toylist.sort((a, b) => {
      return (a.quantity - b.quantity < 0) ? 1 : -1
    })
      .map((item) =>
        <tr key={item.name}>{Object.values(item).map(value => {
          return <td key={item.name + value}>{value}</td>
        })}</tr>
      )
  }
  console.log(toylist)
  return (
    <div>
      <table className="toylist css3-shadow box">
        <thead><tr><th>Name</th><th>Type</th><th>Quantity</th></tr></thead>
        <tbody>
          {toylist}
        </tbody>
      </table>

      <span className="container">
        <label>Sort By Quantity?</label>
        <label className="switch">
          <input type="checkbox" id="checkbox" value={ascending} onChange={toggleAscending} />
          <div className="slider round"></div>
        </label>
      </span>

      <form onSubmit={onSubmit}>
        <fieldset className="toylist css3-shadow box">
          <h3>Add a toy</h3>
          <input className="inputAdd" type="text" value={newName} onChange={onChangeName} placeholder="Name" />
          <input className="inputAdd" type="text" value={newType} onChange={onChangeType} placeholder="Type of toy" />
          <input className="inputAdd" type="number" value={newQuantity} onChange={onChangeQuantity} placeholder="Quantity" />
          <button className="inputAdd">Submit</button>
          <br />
          <br />
        </fieldset>
      </form>
      <br />
      <fieldset className="toylist css3-shadow box">
        <h3>Remove a toy</h3>
        <input type="text" className="inputAdd" value={removeToy} onChange={onChangeRemove} placeholder="Choose toy to remove" />
        <button className="inputAdd" onClick={onRemoveToy}>Remove</button>
      </fieldset>
    </div>
  )
}

export default Toys
