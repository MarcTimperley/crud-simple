import React from 'react'
import './App.css'
import Toys from './Components/Toys'
import Header from './Components/Header'
function App() {
  return (
    <div className="App">
      <Header />
      <Toys />
      <br />
    </div>
  )
}

export default App
