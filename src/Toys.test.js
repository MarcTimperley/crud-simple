import React from 'react'
import ReactDOM from 'react-dom'
import Toys from './Components/Toys'

it('renders list of pets', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Toys />, div)
  ReactDOM.unmountComponentAtNode(div)
})
